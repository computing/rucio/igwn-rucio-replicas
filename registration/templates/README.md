# Templated Offline Registration Jobs
A single `ini` file controls both the k8s deployment and the
`gwrucio_registrar` configuration


To set up a job: 
 1. Upload the diskcache or file list to a publicly visible http URL (we'll get this with `curl`)
 1. Modify the ini file
 1. Run `make_job` to produce a kustomization file (including configmap) and job manifest in a target directory
 1. Add the target directory to the list of bases in the main kustomization file where ssh keys and rucio configmaps are defined

## Notes
 * The job manifest is named after the ini file (`ini->yaml`)
 * The templates are populated by the ini file via a environment variable set by `make_job`

## TODO
 * Replace the `curl` in the `initContainer` with `gfal-copy` and we'll be able to grab cache files from anywhere.
