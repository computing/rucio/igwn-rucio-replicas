#!/bin/bash -e
# run-pmdc.sh
#
# Wrapper to run pmdc
#
# See: https://github.com/jeff-kline/python-pmdc
#
# Workflow:
#	    1. Create LDAS diskcache with pmdc
#	    2. Upload directly to Nautilus k8s cluster (n/a)
#
# James A Clark <james.clark@ligo.org>

frameroot="/archive/frames/O3GK/raw/GEO"

pmdc_home=/home/rucio/pmdc
pmdc="${pmdc_home}/pmdc"
#local_dc="${pmdc_home}/GEO-$(hostname).ldas"
local_dc="O3GK-G-R.ldas"

log_event(){
    event=$1
    log=$(printf '[%s %s %s]\n' "$(hostname)" "$(date --rfc-3339=seconds)")
    echo -e "$log $event"
}

log_event "updating dc: ${local_dc}"

python ${pmdc} /tmp/pmdc_disk_state.cache \
	       --concurrency 10 -e gwf -p ldas \
	       -t /dev/shm ${frameroot} > /tmp/${local_dc}
log_event "Moving dc into position"
mv /tmp/${local_dc} ${local_dc}


