#!/bin/bash -e
# run-pmdc.sh
#
# Wrapper to run pmdc
#
# See: https://github.com/jeff-kline/python-pmdc
#
# Workflow:
#	    1. Create LDAS diskcache with pmdc
#	    2. Upload directly to Nautilus k8s cluster
#
# James A Clark <james.clark@ligo.org>

framedir="/gpfs/data/proc/C10/O3/K1"

pmdc_home=${HOME}/rucio/pmdc
pmdc="${pmdc_home}/pmdc"
local_dc="${pmdc_home}/$(hostname).ldas"

kubectl="${pmdc_home}/kubectl"
kubeconfig="${pmdc_home}/kubeconfig_sa"
KUBE_NAMESPACE="ligo-rucio"
CLIENT_APP="rucio-client"
target_dc="/${KUBE_NAMESPACE}/$(basename ${local_dc})"


log_event(){
    event=$1
    log=$(printf '[%s %s %s]\n' "$(hostname)" "$(date --rfc-3339=seconds)")
    echo -e "$log $event"
}

log_event "updating dc: ${local_dc}"

python ${pmdc} /tmp/pmdc_disk_state.cache \
	       --concurrency 5 -e gwf -p ldas \
	       -t /dev/shm ${framedir} > ${local_dc}


log_event "Getting client pod name"

CLIENT_POD=$(${kubectl} --kubeconfig=${kubeconfig} get pods --namespace ${KUBE_NAMESPACE} -l "app=${CLIENT_APP}" -o jsonpath="{.items[0].metadata.name}")

log_event "Uploading dc to ${target_dc}"
${kubectl}  --kubeconfig=${kubeconfig} cp ${local_dc} ${KUBE_NAMESPACE}/${CLIENT_POD}:${target_dc}
