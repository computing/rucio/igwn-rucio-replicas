-- ******** BEFORE ********
SELECT name,adler32,md5 FROM dids WHERE name=:name;
SELECT name,adler32,md5 FROM replicas WHERE name=:name AND rse_id=:rse_id;

UPDATE dids SET adler32=:adler32, md5=:md5 WHERE name=:name;
UPDATE replicas SET adler32=:adler32, md5=:md5 WHERE name=:name AND rse_id=:rse_id;

-- ******** AFTER ********
SELECT name,adler32,md5 FROM dids WHERE name=:name;
SELECT name,adler32,md5 FROM replicas WHERE name=:name AND rse_id=:rse_id;
