#! /bin/bash -e
rse_id="40d368c9-cec4-438c-b0c5-b3907fa2ad2c"
export PGPASSWORD="rucio"

input_file=new_checksums
while read line; do
    name=$(echo $line | awk '{print $1}')
    echo "INFO: Working on ${name}"
    adler32=$(echo $line | awk '{print $2}')
    md5=$(echo $line | awk '{print $3}')
    psql -U rucio -v name="'${name}'" -v rse_id="'${rse_id}'" -v adler32="'${adler32}'" -v md5="'${md5}'" -f update_checksums.sql --echo-all
done < ${input_file}

